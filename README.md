**Recorrido en Inorden (Inorder Traversal):**
   - Visita el subárbol izquierdo.
   - Visita el nodo actual.
   - Visita el subárbol derecho.

   Ejemplo:
```
    4
   / \
  2   6
 / \ / \
1  3 5  7
```

El recorrido en inorden de este árbol sería: 1, 2, 3, 4, 5, 6, 7.

**Recorrido en Preorden (Preorder Traversal):**
- Visita el nodo actual.
- Visita el subárbol izquierdo.
- Visita el subárbol derecho.

Ejemplo:

```

    4
   / \
  2   6
 / \ / \
1  3 5  7
```
El recorrido en preorden sería: 4, 2, 1, 3, 6, 5, 7.

**Recorrido en Postorden (Postorder Traversal):**
- Visita el subárbol izquierdo.
- Visita el subárbol derecho.
- Visita el nodo actual.

Ejemplo:
```
    4
   / \
  2   6
 / \ / \
1  3 5  7
```

El recorrido en postorden sería: 1, 3, 2, 5, 7, 6, 4.


# Ejercicios Parte A


Para el punto A y el Punto B valide que los postulados sean correctos



## Ejercicio 1: Recorrido en Inorden

Supongamos que tenemos el siguiente recorrido en inorden: 4, 2, 5, 1, 6, 3, 7.

Para construir el árbol a partir de este recorrido, primero debemos entender cómo funciona el recorrido en inorden. El recorrido en inorden visita el subárbol izquierdo, luego el nodo actual y, finalmente, el subárbol derecho.

Para reconstruir el árbol, podemos seguir estos pasos:

1. El primer número en el recorrido inorden (4) será la raíz del árbol.
2. Los números antes de 4 (2 y 5) pertenecen al subárbol izquierdo de 4.
3. Los números después de 4 (1, 6, 3 y 7) pertenecen al subárbol derecho de 4.

Cual sera el arbol resultante?:



## Ejercicio 2: Recorrido en Preorden

Supongamos que tenemos el siguiente recorrido en preorden: 1, 2, 4, 5, 3, 6, 7.

Para construir el árbol a partir de este recorrido, el proceso es similar:

1. El primer número en el recorrido preorden (1) será la raíz del árbol.
2. Los números después de 1 (2, 4 y 5) pertenecen al subárbol izquierdo de 1.
3. Los números después de 5 (3, 6 y 7) pertenecen al subárbol derecho de 1.

Cual sera el arbol resultante?:



## Ejemplo 3: Recorrido en Postorden

Supongamos que tenemos el siguiente recorrido en postorden: 4, 5, 2, 6, 7, 3, 1.

Para construir el árbol a partir de este recorrido, sigue estos pasos:

1. El último número en el recorrido postorden (1) será la raíz del árbol.
2. Los números antes de 1 (4, 5, 2, 6, 7, 3) se dividen en dos partes: los que pertenecen al subárbol izquierdo y los que pertenecen al subárbol derecho.
3. Los números después de 1 (3, 7, 6, 2, 5, 4) son los que pertenecen al subárbol izquierdo.

Cual sera el arbol resultante?:




# Ejercicios Parte B

Supongamos que tenemos el siguiente árbol binario:
```
    1
   / \
  2   3
 / \
4   5
```

Entregue las respuestas de recorrido preorden, postorden e inorden teniendo como apoyo el siguiente codigo, Se suguiere el completar la implementacion para validar la respuesta.

### Recorrido en Preorden

```
def preorden(node):
    if node is not None:
        print(node.value)  # Visita el nodo actual
        preorden(node.left)  # Recorre el subárbol izquierdo
        preorden(node.right)  # Recorre el subárbol derecho

# Llamamos a la función de recorrido en preorden desde la raíz del árbol
arbol = Nodo(1)
arbol.left = Nodo(2)
arbol.right = Nodo(3)
arbol.left.left = Nodo(4)
arbol.left.right = Nodo(5)

preorden(arbol)

```

### Recorrido en Inorden
```
def inorden(node):
    if node is not None:
        inorden(node.left)  # Recorre el subárbol izquierdo
        print(node.value)  # Visita el nodo actual
        inorden(node.right)  # Recorre el subárbol derecho

# Llamamos a la función de recorrido en inorden desde la raíz del árbol
inorden(arbol)

```

### Recorrido en Postorden
```
def postorden(node):
    if node is not None:
        postorden(node.left)  # Recorre el subárbol izquierdo
        postorden(node.right)  # Recorre el subárbol derecho
        print(node.value)  # Visita el nodo actual

# Llamamos a la función de recorrido en postorden desde la raíz del árbol
postorden(arbol)

```



# Nota:

Para todo lo anterior tenga en cuenta:

**Recorrido en Preorden (Preorder Traversal):**
   - En este recorrido, el nodo raíz se visita primero, seguido por el recorrido en preorden del subárbol izquierdo y luego del subárbol derecho. Por lo tanto, el orden es "raíz, izquierda, derecha". Este recorrido visita la raíz antes de los nodos en los subárboles.
   - Útil para crear copias del árbol y evaluar expresiones matemáticas en notación prefija.

**Recorrido en Inorden (Inorder Traversal):**
   - En este recorrido, el nodo raíz se visita después del recorrido en inorden del subárbol izquierdo, pero antes del recorrido en inorden del subárbol derecho. Por lo tanto, el orden es "izquierda, raíz, derecha". Este recorrido visita la raíz entre los nodos en los subárboles izquierdo y derecho.
   - Útil para obtener una secuencia ordenada de los elementos en un árbol de búsqueda binaria.

**Recorrido en Postorden (Postorder Traversal):**
   - En este recorrido, el nodo raíz se visita después del recorrido en postorden del subárbol izquierdo y del subárbol derecho. Por lo tanto, el orden es "izquierda, derecha, raíz". Este recorrido visita la raíz después de los nodos en los subárboles.
   - Útil para liberar la memoria de los nodos en un árbol y evaluar expresiones matemáticas en notación posfija.
